$(document).ready(function () {
    $("body").on("click",".view-detail",function (e) {
        e.preventDefault();
    var id=$(this).attr('id_san');
    var id_loai=$(this).attr('id_loai');
    // alert(id);
        $.ajax({
            url:"/chi-tiet/"+id+"/"+id_loai,
            type:"GET",
            dataType:"html"
        }).done(function(data){
            $(".content-san").html(data);
        }).fail(function(erro){
            console.log(erro)
        });
    });

    $("body").on("click",".view-tr",function (e) {
        e.preventDefault();
        var id=$(this).attr('id_tr');
        var name=$(this).attr('name_tr');
        // alert(id);
        $.ajax({
            url:"/truong/"+id+"/"+name,
            type:"GET",
            dataType:"html"
        }).done(function(data){
            $(".content-san").html(data);
        }).fail(function(erro){
            console.log(erro)
        });
    });

    $("body").on("click",".view-loai",function (e) {
        e.preventDefault();
        var id=$(this).attr('id_loai');
        // alert(id);
        $.ajax({
            url:"/loaisan/"+id,
            type:"GET",
            dataType:"html"
        }).done(function(data){
            $(".content-san").html(data);
        }).fail(function(erro){
            console.log(erro)
        });
    });

    $("body").on("change",".ngay",function () {
        var ngay=$(this).val();
        $("#txt_ngay").val(ngay);
        $("#txt_ngay1").val(ngay);

        var diadiem=$(".name_san").attr("name_add");
        var loaisan=$(".name_san").attr("name_loai");

        $.ajax({
            url:"/khung-gio/",
            type:"GET",
            data:{diadiem:diadiem,loaisan:loaisan},
            dataType:"json"
        }).done(function(data){
            console.log(data.khunggio);
            var sl="<select name=\"slt_khunggio\" class=\"khunggio form-control\">";
            sl+=" <option value=\"\">--Chọn--</option>";
            $.each(data.khunggio,function (i,n) {

                  sl+="<option value="+n.id+">"+n.thoigian+"</option>"

            });
            sl+="</select>";
            $(".test-khunggio").html(sl);
        }).fail(function(erro){
            console.log(erro)
        });

    });
    $("body").on("change",".khunggio",function () {
        var khunggio=$(this).val();

        $(".cart-an").removeAttr('disabled');
        $("#txt_khunggio1").val(khunggio);

        $.ajax({
            url:"/get-khung-gio/",
            type:"GET",
            data:{id:khunggio},
            dataType:"json"
        }).done(function(data){
            console.log(data[0].thoigian);
            $("#txt_khunggio").val(data[0].thoigian);

        }).fail(function(erro){
            console.log(erro)
        });
    });

    $("body").on("click",".cart-an",function (e) {
        e.preventDefault();
        var diadiem=$(".name_san").attr("name_add");
        var loaisan=$(".name_san").attr("name_loai");
        var gia=$(".add-to").attr("price");
        var gia1=$(".add-to").attr("txt_price");
       $("#txt_diadiem").val(diadiem);
       $("#txt_diadiem1").val(diadiem);
       $("#txt_loaisan").val(loaisan);
       $("#txt_loaisan1").val(loaisan);
       $("#txt_thanhtien").val(gia);
       $("#txt_thanhtien1").val(gia1);
       $(".chitiet").css("display","none");
       $(".don-hang").css("display","block");

    });
    $("body").on("click",".back",function () {
        var khunggio=$(this).val();
        $(".chitiet").css("display","block");
        $(".don-hang").css("display","none");
    });

    function getDonhang() {
        $.ajax({
            url:"get-dat-san",
            type:"GET",
            dataType:"json"
        }).done(function (data) {
            div="";
            $.each(data,function (i,n) {
               div+="<div class=\"latest-grid\">\n" +
                   "               <div class=\"news\">\n" +
                   "                   <a href=\"#\"><img class=\"img-responsive\" src='uploads/"+n.images+"'title=\"name\" width='100px' alt=\"\"></a>\n" +
                   "               </div>\n" +
                   "               <div class=\"news-in\">\n" +
                   "                   <h6><a href=\"#\">Trường:"+n.truong+"</a></h6>\n" +
                   "                   <p>Loại sân:"+n.loaisan+"</p>\n" +
                   "                   <ul>\n" +
                   "                       <li>Giá: <span>"+n.thanhtien+".000 vnđ</span> </li><b></b>\n" +
                   "                   </ul>\n" +
                   "               </div>\n" +
                   "               <div class=\"clearfix\"> </div>\n" +
                   "           </div>" ;
            });
            div+="";
        $(".content-siba").html(div);
        }).fail(function (erro) {
            console.log(erro);
        });
    }
    setInterval(getDonhang(),10000);

    // tim kiem
    //
    // $("body").on("click",".tk_truong",function (e) {
    //     var diadiem=$(this).val();
    //     $("#tk_truong").val(diadiem);
    // });
    //
    // $("body").on("click",".tk_loaisan",function (e) {
    //     var loaisan=$(this).val();
    //     $("#tk_loaisan").val(loaisan);
    // });
    //
    // $("body").on("change",".tk_ngay",function () {
    //     var ngay=$(this).val();
    //      $("#tk_date").val(ngay);
    //     var diadiem= $("#tk_truong").val();
    //     var loaisan=$("#tk_loaisan").val();
    //
    //     $.ajax({
    //         url:"/khung-gio/",
    //         type:"GET",
    //         data:{diadiem:diadiem,loaisan:loaisan},
    //         dataType:"json"
    //     }).done(function(data){
    //         console.log(data.khunggio);
    //         var sl="<select name=\"slt_khunggio\" class=\"khunggio form-control\">";
    //         sl+=" <option value=\"\">--Chọn--</option>";
    //         $.each(data.khunggio,function (i,n) {
    //
    //             sl+="<option value="+n.id+">"+n.thoigian+"</option>"
    //
    //         });
    //         sl+="</select>";
    //         $("#tkk_khunggio").html(sl);
    //     }).fail(function(erro){
    //         console.log(erro)
    //     });
    //
    // });
    //
    // $("body").on("change",".tk_khunggio",function () {
    //     var khunggio=$(this).val();
    //
    //     $("#tk_khunggio").val(khunggio);
    //
    //     $.ajax({
    //         url:"/get-khung-gio/",
    //         type:"GET",
    //         data:{id:khunggio},
    //         dataType:"json"
    //     }).done(function(data){
    //         console.log(data[0].thoigian);
    //         $("#tkiem_khunggio").val(data[0].thoigian);
    //
    //     }).fail(function(erro){
    //         console.log(erro)
    //     });
    // });
    //
    // $("body").on("click",'.btn-tk',function (e) {
    //    e.preventDefault();
    //     var diadiem= $("#tk_truong").val();
    //     var loaisan=$("#tk_loaisan").val();
    //     var ngay=$("#tk_date").val();
    //     var thoigian= $("#tkiem_khunggio").val();
    //
    //
    // });
});

