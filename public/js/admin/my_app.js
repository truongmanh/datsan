//loai san
$(document).ready(function(){
    // jQuery methods go here...
    $("#loaisan_form").validate({
       rules:{
            txt_name:"required",
            txt_gia:"required",
            txt_soluong:"required",
            slt_status:"required"
       } ,
        messages:{
            txt_name:"Tên không được để trống",
            txt_gia:"Giá không được để trống",
            txt_soluong:"Giá không được để trống",
            slt_status:"Trạng thái không được để trống"
        },
        submitHandler: function(form) {
            addloaisan();
        }
    });

    $("#sualoaisan_form").validate({
        rules:{
            txt_name:"required",
            txt_gia:"required",
            txt_soluong:"required",
            slt_status:"required"
        } ,
        messages:{
            txt_name:"Tên không được để trống",
            txt_gia:"Giá không được để trống",
            txt_soluong:"Giá không được để trống",
            slt_status:"Trạng thái không được để trống"
        },
        submitHandler: function(form) {
            sualoaisan();
        }
    });

    // thêm loại sân
  function addloaisan() {
      var data=$("#loaisan_form").serialize();
      $.ajax({
          url:"ql_admin/loaisan/them-loai-san",
          type:"POST",
          data:data,
          dataType:"html"
      }).done(function (data) {
          $(".content-loai-san").html(data);
          $("#loaisan_form").trigger('reset');
          // console.log(data);
      }).fail(function (erro) {
          var data=erro.responseJSON.errors;
          var er="<ul>";
          $.each(data, function(key,val) {
              er+="<li>"+val+"</li>";
          });
          er+="</ul>";
          $(".err").css("display","block");
          $(".err").html(er);
          $(".err").delay(4000).slideUp();
      });
  }

    // sửa loại sân
   function sualoaisan() {
       var data=$("#sualoaisan_form").serialize();
       console.log(data);
       $.ajax({
           url:"ql_admin/loaisan/post-loai-san",
           type:"POST",
           data:data,
           dataType:"html"
       }).done(function (data) {

           $(".content-loai-san").html(data);
           $("#loaisan_form").trigger('reset');

       }).fail(function (erro) {
           var data=erro.responseJSON.errors;
           var er="<ul>";
           $.each(data, function(key,val) {
               er+="<li>"+val+"</li>";
           });
           er+="</ul>";
           $(".err").html(er);
       });
   }


    $("body").on('click','.btn-del-san',function (e) {
        e.preventDefault();
        var id=$(this).attr('id_loai_san');

        var checkdel=confirm("Bạn có muốn xóa không?");
        if(checkdel){
            $.ajax({
                url:"ql_admin/loaisan/xoa-loai-san",
                type:"GET",
                data:{id:id},
                dataType:"html"
            }).done(function (data) {
                $(".content-loai-san").html(data);
                // console.log(data);
            }).fail(function (erro) {
                console.log(erro);
            });
        }
        else{return false;}
    });


    $("body").on('click','.btn-edit-san',function (e) {
        e.preventDefault();
        var ids=$(this).attr('id_loai_san');
        $(".content-add").css("display","none");


        $.ajax({
            url:"ql_admin/loaisan/get-loai-san",
            type:"GET",
            data:{id:ids},
            dataType:"json"
        }).done(function (data) {
            $("#txt_id").val(data[0].id);
            $("#txt_name").val(data[0].name);
            $("#gia").val(data[0].gia);
            $("#soluong").val(data[0].soluong);
            if(data[0].status==0){
                sl=" <select name=\"slt_status\" class=\"form-control\">\n" +
                    "                                               <option value=\"0\" selected>Ẩn</option>\n" +
                    "                                               <option value=\"1\">Hiển thị</option>\n" +
                    "                                           </select>";
            }
            else{
                sl=" <select name=\"slt_status\" class=\"form-control\">\n" +
                    "                                               <option value=\"0\">Ẩn</option>\n" +
                    "                                               <option value=\"1\" selected>Hiển thị</option>\n" +
                    "                                           </select>";
            }
            $(".status").html(sl);
            $(".content-edit").css("display","block");
            // console.log(data);
        }).fail(function (erro) {
            console.log(erro);
        });
    });

    $("body").on('click','.btn-huy',function (e) {
        e.preventDefault();
        $(".content-add").css("display","block");
        $(".content-edit").css("display","none");
    });


    //thoi gian


      $("#thoigian_form").validate({
          rules:{
              txt_gio:"required",
              slt_status:"required"
          },
          messages:{
              txt_gio:"Thời gian không được để trống",
              slt_status:"Trạng thái không được để trống"
          },
          submitHandler: function(form) {
              themkhunggio();
          }
      });

    $("#suathoigian").validate({
        rules:{
            txt_gio:"required",
            slt_status:"required"
        },
        messages:{
            txt_gio:"Thời gian không được để trống",
            slt_status:"Trạng thái không được để trống"
        },
        submitHandle:function(form){

            suakhunggio();
        }
    });


    function themkhunggio() {
        var data=$("#thoigian_form").serialize();
        // console.log(data);
        $.ajax({
            url:"ql_admin/thoigian/them-khung-gio",
            type:"POST",
            data:data,
            dataType:"html"
        }).done(function(data){
            $(".content-khunggio").html(data);
            $("#thoigian_form").trigger('reset');
        }).fail(function(erro){
            console.log(erro)
        });
    }

    function suakhunggio() {
        var data=$("#suathoigian").serialize();
        // console.log(data);
        $.ajax({
            url:"ql_admin/thoigian/post-khung-gio",
            type:"POST",
            data:data,
            dataType:"html"
        }).done(function(data){
            $(".content-khunggio").html(data);
            $("#suathoigian_form").trigger('reset');
        }).fail(function(erro){
            console.log(erro)
        });
    }

    $("body").on("click",'.btn-tg-del',function(e){
        e.preventDefault();
        var id=$(this).attr('id_tg');
        // alert(id);
      var checkdel=confirm("Bạn có muốn xóa không?");
      if(checkdel){
          $.ajax({
              url:"ql_admin/thoigian/xoa-khung-gio",
              type:"GET",
              data:{id:id},
              dataType:"html"
          }).done(function(data){
              $(".content-khunggio").html(data);
          }).fail(function(erro){
              console.log(erro)
          });
      }
      else{
          return false;
      }
    });
//


$("body").on("click",".btn-tg-edit",function (e) {
    e.preventDefault();
    var ids=$(this).attr('id_tg');
    $(".content-add").css("display","none");

    $.ajax({
        url:"ql_admin/thoigian/get-khung-gio",
        type:"GET",
        data:{id:ids},
        dataType:"json"
    }).done(function (data) {

        $("#txt_id").val(data[0].id);
        $("#txt_gio").val(data[0].thoigian);
        // $("#txt_gia").val(data[0].gia);
        if(data[0].status==0){
            sl=" <select name=\"slt_status\" class=\"form-control\">\n" +
                "                                               <option value=\"0\" selected>Ẩn</option>\n" +
                "                                               <option value=\"1\">Hiển thị</option>\n" +
                "                                           </select>";
        }
        else{
            sl=" <select name=\"slt_status\" class=\"form-control\">\n" +
                "                                               <option value=\"0\">Ẩn</option>\n" +
                "                                               <option value=\"1\" selected>Hiển thị</option>\n" +
                "                                           </select>";
        }
        $(".status").html(sl);
        $(".content-edit").css("display","block");
        // console.log(data);
    }).fail(function (erro) {
        console.log(erro);
    });
})
$("body").on("click",".btn-tg-sua",function (e) {
    e.preventDefault();
    suakhunggio();
});


// trường


    $("#themtruong_form1").validate({
        rules:{
            txt_name:"required",
            slt_loaisan:"required",
            slt_status:"required",
            txt_mota:"required"
        },
        messages:{
            txt_name:"Tên không được để trống",
            slt_loaisan:"Loại sân không được để trống",
            slt_status:"Trạng thái không được để trống",
            txt_mota:"Mô tả không được để trống",
        },
        submitHandler: function(form) {
            addtruong();
        }
    });


    $("#suatruong_form1").validate({
        rules:{
            txt_name:"required",
            slt_loaisan:"required",
            slt_status:"required",
            txt_mota:"required"
        },
        messages:{
            txt_name:"Tên không được để trống",
            slt_loaisan:"Loại sân không được để trống",
            slt_status:"Trạng thái không được để trống",
            txt_mota:"Mô tả không được để trống",
        },
        submitHandler: function(form) {
            suatruong();
        }
    });

    function addtruong() {

        var form_data = new FormData($("#themtruong_form1")[0]);
        $.ajax({
            url:"ql_admin/truong/them-truong",
            type:"POST",
            data:form_data,
            processData: false,
            contentType: false,
            dataType:"html"
        }).done(function(data){
            $(".content-khunggio").html(data);
            $("#themtruong_form1").trigger('reset');
        }).fail(function(erro){
            console.log(erro)
        });
    }

    $("body").on("click",".btn-del-tr",function(){

        var ids=$(this).attr("trg-id");
       var checkdel=confirm("Bạn có muốn xóa không?");
       if(checkdel){
           $.ajax({
               url:"ql_admin/truong/xoa-truong",
               type:"GET",
               data:{id:ids},
               dataType:"html"
           }).done(function(data){
               $(".content-khunggio").html(data);
           }).fail(function(erro){
               console.log(erro)
           });
       }
else {
           return false;
       }

    });

    $("body").on("click",".btn-edit-tr",function (e) {
        e.preventDefault();
        var ids=$(this).attr("trg-id");
        $(".content-add").css("display","none");
        $.ajax({
            url:"ql_admin/truong/get-edit-truong",
            type:"GET",
            data:{id:ids},
            dataType:"json"
        }).done(function(data){
            console.log(data.truong[0].loaisans[0].id);

            $("#txt_id").val(data.truong[0].id);
            $("#txt_name").val(data.truong[0].name);
            $("#txt_mota").val(data.truong[0].mota);
            $("#txt_imagepre").val(data.truong[0].images);
            im="<img src=uploads/"+data.truong[0].images+" alt=''width='100px' />";
            $(".images").html(im);
            if(data.truong[0].status==0){
                 st="<select name=\"slt_status\" class=\"form-control\">\n" +
                    "                                                <option value=\"0\" selected>Ẩn</option>\n" +
                    "                                                <option value=\"1\">Hiển thị</option>\n" +
                    "                                            </select>"
            }
            else{
                st="<select name=\"slt_status\" class=\"form-control\">\n" +
                    "                                                <option value=\"0\">Ẩn</option>\n" +
                    "                                                <option value=\"1\" selected>Hiển thị</option>\n" +
                    "                                            </select>"
            }
            $(".status").html(st);

            $(".content-edit").css("display","block");
        }).fail(function(erro){
            console.log(erro)
        });
    });

    function suatruong() {

        var form_data = new FormData($("#suatruong_form1")[0]);

        $.ajax({
            url:"ql_admin/truong/post-edit-truong",
            type:"POST",
            data:data,
            processData: false,
            contentType: false,
            dataType:"html"
        }).done(function(data){
            $(".content-khunggio").html(data);
            $("#suatruong_form1").trigger('reset');
        }).fail(function(erro){
            console.log(erro)
        });
    }


//user


    $("#user_form").validate({
        rules:{
            txt_name:"required",
            txt_email:"required",
            txt_mk:"required",
        },
        messages:{
            txt_name:"Tên không được để trống",
            txt_email:"Email không được để trống",
            txt_mk:"Mật khẩu không được để trống",
        },
        submitHandler: function(form) {
            adduser();
        }
    });

    function adduser() {
        var data=$("#user_form").serialize();
        // console.log(data);
        $.ajax({
            url:"ql_admin/users/add-user",
            type:"POST",
            data:data,
            dataType:"html"
        }).done(function(data){
            $(".content-user").html(data);
            $("#user_form").trigger('reset');
        }).fail(function(erro){
            console.log(erro)
        });
    }

    $("body").on("click",".btn-del-user",function (e) {
        e.preventDefault();
        var id=$(this).attr("id_user");
        var checkdel=confirm("Bạn có muốn xóa không?");
        if(checkdel){
            $.ajax({
                url:"ql_admin/users/del-user",
                type:"GET",
                data:{id:id},
                dataType:"html"
            }).done(function(data){
                $(".content-user").html(data);
                $("#user_form").trigger('reset');
            }).fail(function(erro){
                console.log(erro)
            });
        }
        else{
            return false;
        }
    });

    // modal

    $("body").on("click",".btn-edit-dh",function (e) {
        e.preventDefault();
        $("#exampleModal").modal("show");
        var id=$(this).attr("id_loai_san");
        $("#txt_id").val(id);
    });

    $("body").on("click",".btn-sua-dh",function (e) {
        e.preventDefault();
       var data=$("#donhang_form").serialize();
        $.ajax({
            url:"ql_admin/donhang/edit-donhang",
            type:"POST",
            data:data,
            dataType:"html"
        }).done(function(data){
            $(".content-dh").html(data);
            $("#exampleModal").modal("hide");
        }).fail(function(erro){
            console.log(erro)
        });
    });

    $("#tin_form").validate({
        rules:{
            txt_tieude:"required",
            txt_mota:"required",
            slt_status:"required",
            // txt_image:"required"
        },
        messages:{
            txt_tieude:"Tiêu đề không được để trống",
            slt_status:"Trạng thái không được để trống",
            txt_mota:"Mô tả không được để trống",
        },
        submitHandler: function(form) {
            addtin();
        }
    });

    function addtin() {
        var data= new FormData($("#tin_form")[0]);
        $.ajax({
            url: 'ql_admin/tintuc/them-tin',
            type: 'POST',
            data: data,
            dataType: 'html',
            // async: false,
            processData: false,
            contentType: false
        }).done(function (data) {
            $(".content-tin").html(data);
            $("#tin_form").trigger('reset');
        }).fail(function (erro) {
            console.log(erro);
        });
    }

    $("body").on("click",".btn-del-tin",function (e) {
       e.preventDefault();
       var id=$(this).attr('id_tin');
       var check=confirm("Bạn có muốn xóa không?");
       if(check){
           $.ajax({
           url: 'ql_admin/tintuc/xoa-tin',
           type: 'GET',
           data: {id:id},
           dataType: 'html',
       }).done(function (data) {
           $(".content-tin").html(data);
       }).fail(function (erro) {
           console.log(erro);
       });}
    });

    $("body").on("click",".btn-edit-tin",function (e) {
        e.preventDefault();
        $(".content-add").css("display","none");
        var id=$(this).attr('id_tin');
            $.ajax({
                url: 'ql_admin/tintuc/get-tin',
                type: 'GET',
                data: {id:id},
                dataType: 'json',
            }).done(function (data) {
                $("#txt_id").val(data[0].id);
                $("#txt_tieude").val(data[0].title);
                $("#txt_mota").val(data[0].mota);
                $("#txt_imagepre").val(data[0].image);
                ig="uploads/tin/"+data[0].image;
                $(".img").attr('src',ig);
                if(data[0].status==0){
                    st="<select name=\"slt_status\" class=\"form-control\">\n" +
                        "                                                <option value=\"0\" selected>Ẩn</option>\n" +
                        "                                                <option value=\"1\">Hiển thị</option>\n" +
                        "                                            </select>"
                }
                else{
                    st="<select name=\"slt_status\" class=\"form-control\">\n" +
                        "                                                <option value=\"0\">Ẩn</option>\n" +
                        "                                                <option value=\"1\" selected>Hiển thị</option>\n" +
                        "                                            </select>"
                }
                $(".status").html(st);
                    console.log(data);
                $(".content-edit").css("display","block");
            }).fail(function (erro) {
                console.log(erro);
            });
    });

    $("#suatin_form").validate({
        rules:{
            txt_tieude:"required",
            txt_mota:"required",
            slt_status:"required",
            // txt_image:"required"
        },
        messages:{
            txt_tieude:"Tiêu đề không được để trống",
            slt_status:"Trạng thái không được để trống",
            txt_mota:"Mô tả không được để trống",
        },
        submitHandler: function(form) {
            suatin();
        }
    });

    function suatin() {
        var data= new FormData($("#suatin_form")[0]);
        $.ajax({
            url: 'ql_admin/tintuc/sua-tin',
            type: 'POST',
            data: data,
            dataType: 'html',
            // async: false,
            processData: false,
            contentType: false
        }).done(function (data) {
            $(".content-tin").html(data);
            $("#suatin_form").trigger('reset');
        }).fail(function (erro) {
            console.log(erro);
        });
    }
});



