<div class="content-bottom store">
    <h3>Danh sách sân:</h3>
    <div class="bottom-grid">
        @foreach($truong as $item)
            @foreach($item->loaisans as $item1)
        <div class="col-md-4 store-top">
            <div class="bottom-grid-top">
                <a href="#" class="view-detail" id_san="{{$item->id}}" id_loai="{{$item1->id}}"><img class="img-responsive" src="uploads/{{$item->images}}" alt="" >
                    <div class="five">
                        <h6>Sân {{$item1->name}}</h6>
                    </div>
                    <div class="pre">
                        <p>{{$item->name}}</p>
                        <span>{{number_format($item1->gia,3)}} vnđ</span>
                        <div class="clearfix"> </div>
                    </div></a>


            </div>
        </div>
                @endforeach
        @endforeach
        <div class="clearfix"> </div>
    </div>
</div>