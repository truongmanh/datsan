@extends('site.master')
@section('title','Tin tức')
@section('content')
    <div class="col-md-9">
        <div class="content-san">
            <div class="content-bottom list-tin">
                <h3>Tin mới</h3>
                @foreach($tin as $item)
                <div class="row listtin">
                    <div class="col-md-3">
                        <a href="{{route('chtin',['id'=>$item->id,'name'=>$item->title])}}"><img src="uploads/tin/{{$item->image}}" alt="" width="100%"></a>
                    </div>
                    <div class="col-md-9">
                        <a href="{{route('chtin',['id'=>$item->id,'name'=>$item->title])}}"> <h4>{{$item->title}}</h4></a>
                        <p class="tin_mota">{{substr($item->mota,0,200)}}...</p>
                    </div>
                </div>
                    @endforeach
            </div>
        </div>
    </div>
@endsection
