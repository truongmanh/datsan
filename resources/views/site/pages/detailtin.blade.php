@extends('site.master')
@section('title','Chi tiết tin')
@section('content')
    <div class="col-md-9">
        <div class="content-san">
            <div class="content-bottom list-tin">
                <h3>>>Chi tiết tin</h3>
                @foreach($tin as $item)
                    <div class="panel panel-default ">
                        <div class="panel-heading"><h4>>>{{$item->title}}</h4></div>
                        <div class="panel-body content-list-detail">
                            <img src="uploads/tin/{{$item->image}}" alt="" width="500px">
                            <p>{{$item->mota}}
                            </p>
                        </div>
                    </div>

                @endforeach
            </div>
        </div>
    </div>
@endsection
