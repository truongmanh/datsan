@extends('site.master')
@section('title','Trang chủ')
@section('content')
<div class="col-md-9">
   <div class="content-san">
       <div class="shoe">
           <img class="img-responsive" src="site/images/banner1.jpg" alt="" >

       </div>
       <div class="content-bottom">
           <h3>Chọn sân</h3>

           <div class="bottom-grid">
               @foreach($truong as $item)
                   @foreach($item->loaisans as $item1)
                       @if($item1->id==5)
                           <div class="col-md-4 shirt">
                               <div class="bottom-grid-top">
                                   <a href="#" class="view-detail" id_san="{{$item->id}}" id_loai="{{$item1->id}}"><img class="img-responsive" src="uploads/{{$item->images}}" alt="" >
                                       <div class="five">
                                           <h6 >Sân {{$item1->name}}</h6>
                                       </div>
                                       <div class="pre">
                                           <p>{{$item->name}}</p>
                                           <span>{{number_format($item1->gia,3)}} vnđ</span>
                                           <div class="clearfix"> </div>
                                       </div></a>


                               </div>
                           </div>
                       @endif
                   @endforeach
               @endforeach

               <div class="clearfix"> </div>
           </div>
           <div class="bottom-grid">
               @foreach($truong as $item)
                   @foreach($item->loaisans as $item1)
                       @if($item1->id==6)
                           <div class="col-md-4 shirt">
                               <div class="bottom-grid-top">
                                   <a href="#" class="view-detail" id_san="{{$item->id}}" id_loai="{{$item1->id}}"><img class="img-responsive" src="uploads/{{$item->images}}" alt="" >
                                       <div class="five">
                                           <h6 >Sân {{$item1->name}}</h6>
                                       </div>
                                       <div class="pre">
                                           <p>{{$item->name}}</p>
                                           <span>{{number_format($item1->gia,3)}} vnđ</span>
                                           <div class="clearfix"> </div>
                                       </div></a>


                               </div>
                           </div>
                       @endif
                   @endforeach
               @endforeach

               <div class="clearfix"> </div>
           </div>
           <div class="bottom-grid">
               @foreach($truong as $item)
                   @foreach($item->loaisans as $item1)
                       @if($item1->id==7)
                           <div class="col-md-4 shirt">
                               <div class="bottom-grid-top">
                                   <a href="#" class="view-detail" id_san="{{$item->id}}" id_loai="{{$item1->id}}"><img class="img-responsive" src="uploads/{{$item->images}}" alt="" >
                                       <div class="five">
                                           <h6 >Sân {{$item1->name}}</h6>
                                       </div>
                                       <div class="pre">
                                           <p>{{$item->name}}</p>
                                           <span>{{number_format($item1->gia,3)}} vnđ</span>
                                           <div class="clearfix"> </div>
                                       </div></a>


                               </div>
                           </div>
                       @endif
                   @endforeach
               @endforeach

               <div class="clearfix"> </div>
           </div>
       </div>
   </div>
</div>
@endsection
