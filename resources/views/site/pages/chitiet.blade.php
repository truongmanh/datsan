<div class="chitiet">
    @foreach($truong as $item)
        @foreach($item->loaisans as $item1)
            <div class="col-md-7 single-top">
                <img class="etalage_thumb_image img-responsive" src="uploads/{{$item->images}}" alt="" >

            </div>
            <div class="col-md-5 single-top-in">
                <div class="single-para">
                    <h4 class="name_san" name_add="{{$item->name}}" name_loai="{{$item1->name}}">Sân:{{$item->name}} - {{$item1->name}}</h4>
                    <div class="para-grid">
                        <span  class="add-to" price="{{number_format($item1->gia,3)}} vnđ" txt_price="{{$item1->gia}}">Giá:{{number_format($item1->gia,3)}} vnđ</span>
                        <div class="clearfix"></div>
                    </div>
                    @endforeach

                    <div class="available">
                        <h6>Chọn:</h6>
                        <div class="form-group">
                            <label for="">Chọn ngày:</label>
                            <input type="date" name="txt_date" class="ngay form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Chọn Khung giờ:</label>
                           <div class="test-khunggio">
                               <select name="slt_khunggio" class="khunggio form-control">
                                   <option value="">--Chọn--</option>
                                   @foreach($khunggio as $item2)
                                       <option value="{{$item2->id}}">{{$item2->thoigian}}</option>
                                   @endforeach
                               </select>
                           </div>
                        </div>
                    </div>

                   <button class="btn btn-danger cart-an" disabled>Đặt sân</button>
                </div>

            </div>

            <div class="clearfix"> </div>
            <div class="mota">
                <h5>Mô tả:</h5>
                <p>
                    {{$item->mota}}
                </p>
            </div>
        @endforeach
</div>
<div class="don-hang">

        <div class="panel panel-success">
            <div class="panel-heading"> <h3 class="card-title">Nhập thông tin</h3></div>
            <div class="panel-body">
                <form role="form" method="Post" action="{{route('thanhtoan')}}" id="datsan_form">
                    @csrf
                    <div class="col-md-6">
                        <h3>Thông tin cá nhân:</h3>
                        <div class="form-group">
                            <label >Họ tên</label>
                            <input type="text" class="form-control" name="txt_hoten"  placeholder="Nhập họ tên">
                        </div>
                        <div class="form-group">
                            <label >Địa chỉ</label>
                            <input type="text" class="form-control" name="txt_diachi"  placeholder="Nhập họ tên">
                        </div>
                        <div class="form-group">
                            <label >SDT</label>
                            <input type="text" class="form-control" name="txt_sdt" placeholder="Nhập họ tên">
                        </div>
                        <div class="form-group">
                            <label >Số CMT</label>
                            <input type="text" class="form-control" name="txt_cmt" placeholder="Nhập họ tên">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Chi tiết sân:</h3>
                        <div class="form-group">
                            <label >Địa điểm</label>
                            <input type="text" class="form-control" disabled  id="txt_diadiem" placeholder="Nhập họ tên">
                            <input type="hidden" class="form-control"  name="txt_diadiem" id="txt_diadiem1" placeholder="Nhập họ tên">
                        </div>
                        <div class="form-group">
                            <label >Loại sân</label>
                            <input type="text" class="form-control" disabled  id="txt_loaisan" placeholder="Nhập họ tên">
                            <input type="hidden" class="form-control" name="txt_loaisan" id="txt_loaisan1" placeholder="Nhập họ tên">
                        </div>
                        <div class="form-group">
                            <label >Thành tiền</label>
                            <input type="text" class="form-control" disabled  id="txt_thanhtien" placeholder="Nhập họ tên">
                            <input type="hidden" class="form-control"  name="txt_thanhtien" id="txt_thanhtien1" placeholder="Nhập họ tên">
                        </div>
                        <div class="form-group">
                            <label >Ngày</label>
                            <input type="date" class="form-control" disabled   id="txt_ngay" placeholder="Nhập họ tên">
                            <input type="hidden" class="form-control" name="txt_ngay" id="txt_ngay1" placeholder="Nhập họ tên">
                        </div>
                        <div class="form-group">
                            <label >Khung giờ</label>
                            <input type="text" class="form-control" disabled id="txt_khunggio"  placeholder="Nhập họ tên">
                            <input type="hidden" class="form-control"  id="txt_khunggio1" name="txt_khunggio" placeholder="Nhập họ tên">
                        </div>
                    </div>

                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="pull-right btn btn-primary dat_san">Đặt sân</button>
                        <button type="button" class="pull-right btn btn-warning back">Quay lại</button>
                    </div>
                </form>
            </div>
        </div>

</div>
<script>

    $("#datsan_form").validate({
        rules:{
            txt_hoten:"required",
            txt_diachi:"required",
            txt_sdt:"required",
            txt_cmt:"required",
            txt_diadiem:"required",
            txt_loaisan:"required",
            txt_thanhtien:"required",
            txt_ngay:"required",
            txt_khunggio:"required",
        },
        messages:{
            txt_hoten:"Họ tên không được để trống",
            txt_diachi:"Địa chỉ không được để trống",
            txt_sdt:"Số điện thoại không được để trống",
            txt_cmt:"Số chứng minh thư không được để trống",
            txt_diadiem:"Địa điểm không được để trống",
            txt_loaisan:"Loại sân không được để trống",
            txt_thanhtien:"Thành tiền không được để trống",
            txt_ngay:"Ngày không được để trống",
            txt_khunggio:"Khung giờ không được để trống",
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

</script>