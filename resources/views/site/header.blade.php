<div class="header" id="home">
    <div class="header-para">
        <p>Sân bóng các trường đại học thái nguyên</p>
    </div>
    <ul class="header-in">
        <li ><a href="#" style="color: red">Liên hệ:09867546445</a> </li>
    </ul>
    <div class="clearfix"> </div>
</div>
<!---->
<div class="header-top">
    <div class="logo">
        <a href="index.html"><img src="uploads/logo.png" alt=""></a>
    </div>
    <div class="header-top-on">
        <ul class="social-in">
            <li><a href="#"><i> </i></a></li>
            <li><a href="#"><i class="ic"> </i></a></li>
            <li><a href="#"><i class="ic1"> </i></a></li>
            <li><a href="#"><i class="ic2"> </i></a></li>
            <li><a href="#"><i class="ic3"> </i></a></li>
        </ul>
    </div>
    <div class="clearfix"> </div>
</div>
<div class="header-bottom">
    <div class="top-nav">

        <ul class="megamenu skyblue">
            <li><a class="pink"  href="/">Trang chủ</a></li>
            <li class="active grid"><a  href="/">Trường</a>
                <div class="megapanel">
                    <div class="row">

                        <div class="col1">
                            <div class="h_nav">

                                <ul>
                                    @foreach($truong as $item)
                                    <li><a href="#" id_tr="{{$item->id}}" name_tr="{{$item->name}}" class="view-tr">{{$item->name}}</a></li>
                                    @endforeach
                                </ul>

                            </div>
                        </div>

                    </div>
                </div>
            </li>
            <li class="grid"><a  href="/">Loại sân</a>
                <div class="megapanel">
                    <div class="row">

                        <div class="col1">
                            <div class="h_nav">
                                <ul>
                                    @foreach($loaisan as $item)
                                    <li><a href="#" id_loai="{{$item->id}}" class="view-loai">{{$item->name}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </li>
            <li><a class="pink"  href="{{route('tin')}}">Tin Tức</a></li>
        </ul>
    </div>
    <div class="clearfix"> </div>
</div>
<div class="page">

</div>