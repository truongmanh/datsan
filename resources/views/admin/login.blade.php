<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <base href="{{asset('')}}">
    <title>Login</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="css/admin/login.css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
<div class = "container">
    <div class="wrapper">
        <form action="{{route('postlogin')}}" method="Post" name="Login_Form" class="form-signin">
            <h3 class="form-signin-heading">Đăng nhập</h3>
            <hr class="colorgraph"><br>
            @csrf
            <input type="text" class="form-control" name="Username" placeholder="Tên đăng nhập" required="" autofocus="" />
            <input type="password" class="form-control" name="Password" placeholder="Mật khẩu" required=""/>

            <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Login" type="Submit">Đăng nhập</button>
        </form>
    </div>
</div>
</body>
</html>