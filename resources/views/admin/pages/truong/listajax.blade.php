<div class="card card-warning">
    <div class="card-header">
        <h3 class="card-title">Danh sách trường ĐH</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>STT</th>
                <th>Tên trường</th>
                <th>Hành động</th>
            </tr>
            <?php $stt=1 ?>
            @foreach($truong as $item)
                <tr>
                    <td>{{$stt++}}</td>
                    <td>{{$item->name}}</td>
                    <td><button class="btn btn-primary btn-del-tr" trg-id="{{$item->id}}">Xóa</button> <button class="btn btn-warning btn-edit-tr" trg-id="{{$item->id}}">Sửa</button></td>
                </tr>
            @endforeach
        </table>
    </div>
    <!-- /.card-body -->
</div>