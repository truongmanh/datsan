@extends('admin.master')
@section('title','Quản lý trường ĐH')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-5">
                   <div class="content-add">
                       <div class="card card-primary">
                           <div class="card-header">
                               <h3 class="card-title">Thêm trường ĐH</h3>
                           </div>
                           <!-- /.card-header -->
                           <!-- form start -->
                           <form method="Post" role="form" id="themtruong_form1" enctype="multipart/form-data">
                               @csrf
                               <div class="card-body">
                                   <div class="form-group">
                                       <label>Tên trường</label>
                                       <input type="text" class="form-control" placeholder="nhập tên trường" name="txt_name">
                                   </div>
                                   <div class="form-group">
                                       <label >Trạng thái</label>
                                       <select name="slt_status" class="form-control">
                                           <option value="">--Chọn--</option>
                                           <option value="0">Ẩn</option>
                                           <option value="1">Hiển thị</option>
                                       </select>
                                   </div>
                                   <div class="form-group">
                                       <label >Loại sân:</label>
                                       @foreach($ls as $item)
                                           <input type="checkbox" name="slt_loaisan[]" value="{{$item->id}}">{{$item->name}}
                                       @endforeach
                                   </div>
                                    <div class="form-group">
                                        <label >Mô tả:</label>
                                        <textarea class="textarea" name="txt_mota" placeholder="Place some text here"
                                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    </div>
                                   <div class="form-group">
                                       <label>Hình ảnh</label>
                                       <input type="file" class="form-control" placeholder="nhập tên trường" name="txt_image">
                                   </div>
                               </div>
                               <!-- /.card-body -->

                               <div class="card-footer">
                                   <button type="submit" class="btn btn-primary">Thêm</button>
                               </div>
                           </form>
                       </div>
                   </div>
                    <div class="content-edit">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Sửa trường ĐH</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="Post" role="form" id="suatruong_form1" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" class="form-control" placeholder="nhập tên trường" name="txt_id" id="txt_id">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Tên trường</label>
                                        <input type="text" class="form-control" placeholder="nhập tên trường" name="txt_name" id="txt_name">
                                    </div>
                                    <div class="form-group">
                                        <label >Trạng thái</label>
                                        <div class="status">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label >Loại sân</label>
                                        @foreach($ls as $item)
                                            <input type="checkbox" name="slt_loaisan[]" value="{{$item->id}}" checked>{{$item->name}}
                                        @endforeach
                                    </div>
                                    <div class="form-group">
                                        <label >Mô tả:</label>
                                        <textarea class="textarea" id="txt_mota" name="txt_mota" placeholder="Place some text here"
                                                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Hình ảnh</label>
                                        <input type="file" class="form-control" placeholder="nhập tên trường" name="txt_image">
                                        <input type="hidden" class="form-control" placeholder="nhập tên trường" id="txt_imagepre" name="txt_imagepre">
                                    </div>
                                    <div class="images">

                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Sửa</button>
                                    <button type="button" class="btn btn-warning btn-huy">Hủy</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-7">
                    <div class="content-khunggio">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Danh sách trường ĐH</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên trường</th>
                                        <th>Hành động</th>
                                    </tr>
                                    <?php $stt=1 ?>
                                    @foreach($truong as $item)
                                        <tr>
                                            <td>{{$stt++}}</td>
                                            <td>{{$item->name}}</td>
                                            <td><button class="btn btn-primary btn-del-tr" trg-id="{{$item->id}}">Xóa</button> <button class="btn btn-warning btn-edit-tr" trg-id="{{$item->id}}">Sửa</button></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <!-- /.col -->
            </div>
        </div>
    </section>
    @endsection