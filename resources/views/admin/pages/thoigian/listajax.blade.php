<div class="card card-warning">
    <div class="card-header">
        <h3 class="card-title">Danh sách khung giờ</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table class="table table-bordered">
            <tr>
                <th>STT</th>
                <th>Khung gio</th>
                <th>Trạng thái</th>
                <th>Hành động</th>
            </tr>
            <?php $stt=1 ?>
            @foreach($thoigian as $item)
            <tr>
                <td>{{$stt++}}</td>
                <td>{{$item->thoigian}}</td>
                <td>@if($item->status==0){{"Ẩn"}}
                @else {{"Hiện"}}
                @endif</td>
                <td>
                    <button type="button" class="btn btn-primary btn-tg-del" id_tg="{{$item->id}}">Xóa</button>
                    <button type="button" class="btn btn-warning btn-tg-edit" id_tg="{{$item->id}}">Sửa</button>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
    <!-- /.card-body -->
</div>