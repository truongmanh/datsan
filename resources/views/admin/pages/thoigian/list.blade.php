@extends('admin.master')
@section('title','Quản lý thời gian')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4">
                    <div class="content-add">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Thêm khung giờ</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="POST"  role="form1" id="thoigian_form">

                                <div class="card-body">
                                    @csrf
                                    <div class="err"></div>
                                    <div class="form-group">
                                        <label>Khung giờ</label>
                                        <input type="text" class="form-control" placeholder="nhập khung gio" name="txt_gio">
                                    </div>
                                    <div class="form-group">
                                        <label >Trạng thái</label>
                                        <select name="slt_status" class="form-control">
                                            <option value="">--Chọn--</option>
                                            <option value="0">Ẩn</option>
                                            <option value="1">Hiển thị</option>
                                        </select>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-add-tg">Thêm</button>
                                </div>
                            </form>
                                <!-- /.card-body -->
                        </div>
                    </div>
                    <div class="content-edit">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Sửa khung giờ</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="post" role="form2" id="suathoigian">
                                @csrf
                                <input type="hidden" class="form-control" placeholder="nhập khung gio" name="id" id="txt_id">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Khung giờ</label>
                                        <input type="text" class="form-control" placeholder="nhập khung gio" name="txt_gio" id="txt_gio">
                                    </div>
                                    <div class="form-group">
                                        <label >Trạng thái</label>
                                       <div class="status">

                                           <select name="slt_status" class="form-control">
                                               <option value="">--Chọn--</option>
                                               <option value="0">Ẩn</option>
                                               <option value="1">Hiển thị</option>
                                           </select>
                                       </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="button" class="btn btn-primary btn-tg-sua">Sửa</button>
                                    <button type="button" class="btn btn-warning btn-huy">Hủy</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-8">
                   <div class="content-khunggio">
                       <div class="card card-warning">
                           <div class="card-header">
                               <h3 class="card-title">Danh sách khung giờ</h3>
                           </div>
                           <!-- /.card-header -->
                           <div class="card-body">
                               <table class="table table-bordered">
                                   <tr>
                                       <th>STT</th>
                                       <th>Khung giờ</th>
                                       <th>Trạng thái</th>
                                       <th>Hành động</th>
                                   </tr>
                                   <?php $stt=1 ?>
                                   @foreach($thoigian as $item)
                                       <tr>
                                           <td>{{$stt++}}</td>
                                           <td>{{$item->thoigian}}</td>
                                           <td>@if($item->status==0){{"Ẩn"}}
                                               @else {{"Hiện"}}
                                               @endif</td>
                                           <td>
                                               <button type="button" class="btn btn-primary btn-tg-del" id_tg="{{$item->id}}">Xóa</button>
                                               <button type="button" class="btn btn-warning btn-tg-edit" id_tg="{{$item->id}}">Sửa</button>
                                           </td>
                                       </tr>
                                   @endforeach
                               </table>
                           </div>
                           <!-- /.card-body -->
                       </div>
                   </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <!-- /.col -->
            </div>
        </div>
    </section>
@endsection