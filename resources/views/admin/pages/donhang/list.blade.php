@extends('admin.master')
@section('title','Quản lý đặt sân')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row ">
                <div class="content-dh">
                    <div class="col-12">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Danh sách đặt sân</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="content-loai-san">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>STT</th>
                                            <th>Trường</th>
                                            <th>Loại sân</th>
                                            <th>Thời gian</th>
                                            <th>Họ tên</th>
                                            <th>SĐT</th>
                                            <th>Số Cmt</th>
                                            <th>Trạng thái</th>
                                            <th>Hành động</th>
                                        </tr>
                                        <?php $stt=0 ?>
                                        @foreach($dh as $item)
                                            <tr>
                                                <td>{{++$stt}}</td>
                                                <td>{{$item->truong}}</td>
                                                <td>{{$item->loaisan}}</td>
                                                <td>{{$item->thoigian}}</td>
                                                <td>{{$item->ten}}</td>
                                                <td>{{$item->sdt}}</td>
                                                <td>{{$item->cmt}}</td>
                                                <td>@if($item->status==1)
                                                        {{"Đã thanh toán"}}
                                                    @else {{"Chưa thanh toán"}}
                                                    @endif
                                                </td>
                                                <td><button class="btn btn-warning btn-edit-dh" id_loai_san="{{$item->id}}" >Sửa</button></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <div class="col-12">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Danh sách đặt sân(đã thanh toán)</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <div class="content-loai-san">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>STT</th>
                                            <th>Trường</th>
                                            <th>Loại sân</th>
                                            <th>Thời gian</th>
                                            <th>Họ tên</th>
                                            <th>SĐT</th>
                                            <th>Số Cmt</th>
                                            <th>Trạng thái</th>

                                        </tr>
                                        <?php $stt=0 ?>
                                        @foreach($dhh as $item)
                                            <tr>
                                                <td>{{++$stt}}</td>
                                                <td>{{$item->truong}}</td>
                                                <td>{{$item->loaisan}}</td>
                                                <td>{{$item->thoigian}}</td>
                                                <td>{{$item->ten}}</td>
                                                <td>{{$item->sdt}}</td>
                                                <td>{{$item->cmt}}</td>
                                                <td>@if($item->status==1)
                                                        {{"Đã thanh toán"}}
                                                    @else {{"Chưa thanh toán"}}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.col -->
                <!-- /.col -->
            </div>
        </div>
    </section>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sửa đơn hàng</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST"  role="form" id="donhang_form">
                            @csrf
                        <input type="hidden" name="txt_id" id="txt_id" value="">
                            <div class="form-group">
                                <label>Trạng thái</label>
                               <div class="sldonhang">
                                   <select name="slt_status" class="form-control">
                                       <option value="">--Chọn--</option>
                                       <option value="0">Chưa thanh toán</option>
                                       <option value="1">Đã thanh toán</option>
                                   </select>
                               </div>
                            </div>
                        <!-- /.card-body -->
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-sua-dh">Sửa</button>
                </div>
            </div>
        </div>
    </div>
@endsection
