<div class="col-12">
    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Danh sách đặt sân</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="content-loai-san">
                <table class="table table-bordered">
                    <tr>
                        <th>STT</th>
                        <th>Trường</th>
                        <th>Loại sân</th>
                        <th>Thời gian</th>
                        <th>Họ tên</th>
                        <th>SĐT</th>
                        <th>Số Cmt</th>
                        <th>Trạng thái</th>
                        <th>Hành động</th>
                    </tr>
                    <?php $stt=0 ?>
                    @foreach($dh as $item)
                        <tr>
                            <td>{{++$stt}}</td>
                            <td>{{$item->truong}}</td>
                            <td>{{$item->loaisan}}</td>
                            <td>{{$item->thoigian}}</td>
                            <td>{{$item->ten}}</td>
                            <td>{{$item->sdt}}</td>
                            <td>{{$item->cmt}}</td>
                            <td>@if($item->status==1)
                                    {{"Đã thanh toán"}}
                                @else {{"Chưa thanh toán"}}
                                @endif
                            </td>
                            <td><button class="btn btn-warning btn-edit-dh" id_loai_san="{{$item->id}}" >Sửa</button></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<div class="col-12">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Danh sách đặt sân(đã thanh toán)</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="content-loai-san">
                <table class="table table-bordered">
                    <tr>
                        <th>STT</th>
                        <th>Trường</th>
                        <th>Loại sân</th>
                        <th>Thời gian</th>
                        <th>Họ tên</th>
                        <th>SĐT</th>
                        <th>Số Cmt</th>
                        <th>Trạng thái</th>

                    </tr>
                    <?php $stt=0 ?>
                    @foreach($dhh as $item)
                        <tr>
                            <td>{{++$stt}}</td>
                            <td>{{$item->truong}}</td>
                            <td>{{$item->loaisan}}</td>
                            <td>{{$item->thoigian}}</td>
                            <td>{{$item->ten}}</td>
                            <td>{{$item->sdt}}</td>
                            <td>{{$item->cmt}}</td>
                            <td>@if($item->status==1)
                                    {{"Đã thanh toán"}}
                                @else {{"Chưa thanh toán"}}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>