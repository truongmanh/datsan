<div class="col-12">
    <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Danh sách tin</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div class="content-loai-san">
                <table class="table table-bordered">
                    <tr>
                        <th>STT</th>
                        <th>Hình ảnh</th>
                        <th>Tiêu đề</th>
                        <th>Trạng thái</th>
                        <th>Hành động</th>
                    </tr>
                    <?php $stt=0 ?>
                    @foreach($tin as $item)
                    <tr>
                    <td>{{++$stt}}</td>
                    <td><img src="uploads/tin/{{$item->image}}" alt="" width="80px"></td>
                    <td>{{$item->title}}</td>
                    <td>@if($item->status==1)
                    {{"Hiện"}}
                    @else {{"Ẩn"}}
                    @endif
                    </td>
                    <td><button type="button" class="btn btn-success btn-del-tin" id_tin="{{$item->id}}">Xóa</button> <button class="btn btn-warning btn-edit-tin" id_tin="{{$item->id}}">Sửa</button></td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>