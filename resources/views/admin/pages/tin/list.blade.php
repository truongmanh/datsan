@extends('admin.master')
@section('title','Quản lý tin')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-8 offset-2">
                    <div class="content-add">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title title_loaisan">Thêm tin</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="POST"  role="form" id="tin_form" enctype="multipart/form-data">

                                <div class="card-body">
                                    @csrf
                                    <div class="err"></div>
                                    <div class="form-group">
                                        <label>Tiêu đề</label>
                                        <input type="text" class="form-control" name="txt_tieude" placeholder="nhập tiêu đề">
                                    </div>
                                    <div class="form-group">
                                        <label>Mô tả</label>
                                        <textarea class="form-control"  name="txt_mota" id="" cols="30" rows="10">

                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Hình ảnh</label>
                                        <input type="file" class="form-control"  name="txt_image" placeholder="nhập gia">
                                    </div>
                                    <div class="form-group">
                                        <label >Trạng thái</label>
                                        <div id="status">
                                            <select name="slt_status" class="form-control">
                                                <option value="">--Chọn--</option>
                                                <option value="0">Ẩn</option>
                                                <option value="1">Hiển thị</option>
                                            </select>
                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-add">Thêm</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="content-edit">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Sửa tin</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="POST"  role="form1" id="suatin_form" enctype="multipart/form-data">

                                <div class="card-body">
                                    @csrf
                                    <div class="err"></div>
                                    <input type="hidden" class="form-control" name="id" id="txt_id" placeholder="nhập tên loại sân">
                                    <div class="form-group">
                                        <label>Tiêu đề</label>
                                        <input type="text" class="form-control" name="txt_tieude" id="txt_tieude" placeholder="nhập tiêu đề" >
                                    </div>
                                    <div class="form-group">
                                        <label>Mô tả</label>
                                        <textarea class="form-control"  name="txt_mota" id="txt_mota" cols="30" rows="10">

                                        </textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Hình ảnh</label>
                                        <input type="file" class="form-control" name="txt_image"  >
                                        <input type="hidden" class="form-control" name="txt_imagepre" id="txt_imagepre" >

                                    </div>
                                    <div>
                                        <img class="img" src="" alt="" width="80px">
                                    </div>
                                    <div class="form-group">
                                        <label >Trạng thái</label>
                                        <div class="status">

                                        </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Sửa</button>
                                    <button type="button" class="btn btn-warning btn-huy">Hủy</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row content-tin">
                   <div class="col-12">

                       <div class="card card-warning">
                           <div class="card-header">
                               <h3 class="card-title">Danh sách tin</h3>
                           </div>
                           <!-- /.card-header -->
                           <div class="card-body">
                               <div class="content-loai-san">
                                   <table class="table table-bordered">
                                       <tr>
                                           <th>STT</th>
                                           <th>Hình ảnh</th>
                                           <th>Tiêu đề</th>
                                           <th>Trạng thái</th>
                                           <th>Hành động</th>
                                       </tr>
                                       <?php $stt=0 ?>
                                       @foreach($tin as $item)
                                           <tr>
                                               <td>{{++$stt}}</td>
                                               <td><img src="uploads/tin/{{$item->image}}" alt="" width="80px"></td>
                                               <td>{{$item->title}}</td>
                                               <td>@if($item->status==1)
                                                       {{"Hiện"}}
                                                   @else {{"Ẩn"}}
                                                   @endif
                                               </td>
                                               <td><button type="button" class="btn btn-success btn-del-tin" id_tin="{{$item->id}}">Xóa</button> <button class="btn btn-warning btn-edit-tin" id_tin="{{$item->id}}">Sửa</button></td>
                                           </tr>
                                       @endforeach
                                   </table>
                               </div>
                           </div>
                           <!-- /.card-body -->
                       </div>
                       <!-- /.card -->

               </div>
            </div>
        </div>
    </section>

@endsection
