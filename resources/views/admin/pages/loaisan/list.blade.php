@extends('admin.master')
@section('title','Quản lý loại sân')
@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4">
                    <div class="content-add">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title title_loaisan">Thêm loại sân</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form method="POST"  role="form" id="loaisan_form">

                                <div class="card-body">
                                    @csrf
                                    <div class="err"></div>
                                    <div class="form-group">
                                        <label>Tên loại sân</label>
                                        <input type="text" class="form-control" name="txt_name" placeholder="nhập tên loại sân">
                                    </div>
                                    <div class="form-group">
                                        <label>Giá</label>
                                        <input type="text" class="form-control"  name="txt_gia" placeholder="nhập gia">
                                    </div>
                                    <div class="form-group">
                                        <label>Số lượng</label>
                                        <input type="text" class="form-control"  name="txt_soluong" placeholder="nhập số lượng">
                                    </div>
                                    <div class="form-group">
                                        <label >Trạng thái</label>
                                       <div id="status">
                                           <select name="slt_status" class="form-control">
                                               <option value="">--Chọn--</option>
                                               <option value="0">Ẩn</option>
                                               <option value="1">Hiển thị</option>
                                           </select>
                                       </div>
                                    </div>

                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-add">Thêm</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="content-edit">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Sửa loại sân</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                                <form method="POST"  role="form1" id="sualoaisan_form">

                                    <div class="card-body">
                                        @csrf
                                        <div class="err"></div>
                                        <input type="hidden" class="form-control" name="id" id="txt_id" placeholder="nhập tên loại sân">
                                        <div class="form-group">
                                            <label>Tên loại sân</label>
                                            <input type="text" class="form-control" name="txt_name" id="txt_name" placeholder="nhập tên loại sân" >
                                        </div>
                                        <div class="form-group">
                                            <label>Giá</label>
                                            <input type="text" class="form-control" id="gia" name="txt_gia" placeholder="nhập gia">
                                        </div>
                                        <div class="form-group">
                                            <label>Số lượng</label>
                                            <input type="text" class="form-control" id="soluong" name="txt_soluong" placeholder="nhập số lượng">
                                        </div>
                                        <div class="form-group">
                                            <label >Trạng thái</label>
                                           <div class="status">

                                           </div>
                                        </div>

                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary">Sửa</button>
                                        <button type="button" class="btn btn-warning btn-huy">Hủy</button>
                                    </div>
                                </form>
                        </div>

                    </div>
                </div>
                <div class="col-8">
                    <div class="card card-warning">
                        <div class="card-header">
                            <h3 class="card-title">Danh sách loại sân</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                           <div class="content-loai-san">
                               <table class="table table-bordered">
                                   <tr>
                                       <th>STT</th>
                                       <th>Loại sân</th>
                                       <th>Trạng thái</th>
                                       <th>Hành động</th>
                                   </tr>
                                   <?php $stt=0 ?>
                                   @foreach($loaisan as $item)
                                       <tr>
                                           <td>{{++$stt}}</td>
                                           <td>{{$item->name}}</td>
                                           <td>@if($item->status==1)
                                                   {{"Hiện"}}
                                               @else {{"Ẩn"}}
                                               @endif
                                           </td>
                                           <td><button type="button" class="btn btn-success btn-del-san" id_loai_san="{{$item->id}}">Xóa</button> <button class="btn btn-warning btn-edit-san" id_loai_san="{{$item->id}}">Sửa</button></td>
                                       </tr>
                                   @endforeach
                               </table>
                           </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                <!-- /.col -->
            </div>
        </div>
    </section>

@endsection
