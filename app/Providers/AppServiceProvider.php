<?php

namespace App\Providers;

use App\Khunggio;
use App\Loaisan;
use App\Truong;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use DB;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
       View::composer(['site.header','site.sidebar'],function($view){
           $truong = DB::table('truongs')->get();
            $khunggio=Khunggio::all();
           $loaisan=Loaisan::all();
           $view->with(['truong'=>$truong,'loaisan'=>$loaisan,'khunggio'=>$khunggio]);
       });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
