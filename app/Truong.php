<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Truong extends Model
{
    protected $table="truongs";

    public function loaisans(){
        return $this->belongsToMany('App\Loaisan');
    }
}
