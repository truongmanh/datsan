<?php

namespace App\Http\Controllers;

use App\vitri;
use Illuminate\Http\Request;

class VitriController extends Controller
{

    public function index()
    {
        $vitri=vitri::orderBy('id','desc')->get();
        return view('admin.pages.vitri.list',compact('vitri'));
    }

    public function getList()
    {
        $vitri=vitri::orderBy('id','desc')->get();
        return view('admin.pages.vitri.listajax',compact('vitri'));
    }

    public function addVitri(Request $request)
    {
        try{
            $vitri=new vitri();
            $vitri->name=$request->txt_name;
            $vitri->status=$request->slt_status;
            $vitri->save();
            return $this->getList();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function delete(Request $request)
    {
        $vitri=vitri::find($request->id);
        $vitri->delete();
        return $this->getList();
    }

    public function getEdit(Request $request)
    {
        $vitri=vitri::where('id',$request->id)->get();
        return response()->json($vitri);
    }

    public function postEdit(Request $request){
        try{
            $vitri=vitri::find($request->id);
            $vitri->name=$request->txt_name;
            $vitri->status=$request->slt_status;
            $vitri->update();
            return $this->getList();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }
}
