<?php

namespace App\Http\Controllers;

use App\Bill;
use Illuminate\Http\Request;
use DB;

class DonhangController extends Controller
{
    public function index(){
        $dh=DB::table('donhang')
            ->join('khunggio', 'khunggio.id', '=', 'donhang.khunggio')
            ->select('donhang.*','khunggio.thoigian')
            ->where('donhang.status',0)->orderBy('donhang.id','desc')->limit(10)->get();

        $dhh=DB::table('donhang')
            ->join('khunggio', 'khunggio.id', '=', 'donhang.khunggio')
            ->select('donhang.*','khunggio.thoigian')
            ->where('donhang.status',1)->orderBy('donhang.id','desc')->limit(10)->get();

        return view('admin.pages.donhang.list',compact('dh','dhh'));
    }

    public function getList(){
        $dh=DB::table('donhang')
            ->join('khunggio', 'khunggio.id', '=', 'donhang.khunggio')
            ->select('donhang.*','khunggio.thoigian')
            ->where('donhang.status',0)->orderBy('donhang.id','desc')->limit(10)->get();
        $dhh=DB::table('donhang')
            ->join('khunggio', 'khunggio.id', '=', 'donhang.khunggio')
            ->select('donhang.*','khunggio.thoigian')
            ->where('donhang.status',1)->orderBy('donhang.id','desc')->limit(10)->get();
        return view('admin.pages.donhang.listajax',compact('dh','dhh'));
    }

    public function postDonhang(Request $request)
    {
        $dh=Bill::find($request->txt_id);
        $dh->status=$request->slt_status;
        $dh->update();
        return $this->getList();
    }
}
