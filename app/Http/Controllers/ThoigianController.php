<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddKhunggioRequest;
use App\Khunggio;
use Illuminate\Http\Request;

class ThoigianController extends Controller
{
    public function index()
    {
        $thoigian=Khunggio::orderBy('id','desc')->get();
       return view('admin.pages.thoigian.list',compact('thoigian'));
    }

    public function listajax()
    {
        $thoigian=Khunggio::orderBy('id','desc')->get();
        return view('admin.pages.thoigian.listajax',compact('thoigian'));
    }

    public function addTime(Request $request)
    {
        try{
            $thoigian=new Khunggio();
            $thoigian->thoigian=$request->txt_gio;
            $thoigian->status=$request->slt_status;
            $thoigian->save();
            return $this->listajax();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function delete(Request $request)
    {
        $tg=Khunggio::find($request->id);
        $tg->delete();
        return $this->listajax();
    }

    public function getEdit(Request $request)
    {
        $id=$request->id;
        $tg=Khunggio::where('id',$id)->get();
        return response()->json($tg);
    }

    public function postEdit(Request $request){
        try{
            $id=$request->id;
            $thoigian=Khunggio::find($id);
            $thoigian->thoigian=$request->txt_gio;
            $thoigian->status=$request->slt_status;
            $thoigian->update();
            return $this->listajax();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }
}
