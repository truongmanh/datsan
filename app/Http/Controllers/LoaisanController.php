<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddLoaisanRequest;
use App\Http\Requests\EditLoaisanRequest;
use App\Loaisan;
use Illuminate\Http\Request;

class LoaisanController extends Controller
{
    public function index()
    {
        $loaisan=Loaisan::orderBy('id','desc')->get();
        return view('admin.pages.loaisan.list',compact('loaisan'));
    }

    public function getList(){
        $loaisan=Loaisan::orderBy('id','desc')->get();
        return view('admin.pages.loaisan.listajax',compact('loaisan'));
    }


    public function create(Request $request){
        try{
            $loaisan=new Loaisan();
            $loaisan->name=$request->txt_name;
            $loaisan->gia=$request->txt_gia;
            $loaisan->soluong=$request->txt_soluong;
            $loaisan->status=$request->slt_status;
            $loaisan->save();
            return $this->getList();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function delete(Request $request){
      try{
          $loaisan=Loaisan::findorFail($request->id);
          $loaisan->delete();
          return $this->getList();
      }
      catch (\Exception $e){
          return $e->getMessage();
      }
    }

    public function getUpdated(Request $request)
    {
        try{
            $loaisan=Loaisan::where('id',$request->id)->get();
            return response()->json($loaisan);
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function postUpdated(EditLoaisanRequest $request)
    {
        try{
            $loaisan=Loaisan::find($request->id);
            $loaisan->name=$request->txt_name;
            $loaisan->gia=$request->txt_gia;
            $loaisan->soluong=$request->txt_soluong;
            $loaisan->status=$request->slt_status;
            $loaisan->update();
            return $this->getList();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }
}
