<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Khunggio;
use App\Tin;
use App\Truong;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $truong=Truong::with('loaisans')->limit(3)->get();
        return view('site.pages.trangchu',compact('truong'));
    }

    public function getDetail(Request $request)
    {
        $id_l=$request->id_loai;
        $truong=Truong::with(array('loaisans'=> function ($query) use ($id_l) {
            $query->where('loaisan_id','=',$id_l);
        }))->where('id',$request->id)->get();

        $khunggio=Khunggio::all();

        return view('site.pages.chitiet',compact('truong','khunggio'));
    }

    public function getTruong(Request $request)
    {
        $truong=Truong::with('loaisans')->where('id',$request->id)->get();
        return view('site.pages.listtruong',compact('truong'));
    }

    public function getLoaisan(Request $request)
    {
        $id_l=$request->id;
        $truong=Truong::with(array('loaisans'=> function ($query) use ($id_l) {
            $query->where('loaisan_id','=',$id_l);
        }))->get();
        return view('site.pages.listloai',compact('truong'));
    }

    public function getDathang(Request $request)
    {
        $dh=new Bill();
        $dh->truong=$request->txt_diadiem;
        $dh->loaisan=$request->txt_loaisan;
        $dh->khunggio=$request->txt_khunggio;
        $dh->ten=$request->txt_hoten;
        $dh->sdt=$request->txt_sdt;
        $dh->cmt=$request->txt_cmt;
        $dh->diachi=$request->txt_diachi;
        $dh->thanhtien=$request->txt_thanhtien;
        $dh->status=0;
        $dh->date=Carbon::parse($request->txt_ngay);
        $dh->save();
        return view('site.pages.dathang');
    }

    public function getKhunggio(Request $request){
        $diadiem=$request->diadiem;
        $loaisan=$request->loaisan;

        if(strcmp("11 người",$loaisan)==0){
            $khunggio11=Bill::select('khunggio')->where('truong','like',$diadiem)
                ->where('loaisan','like','11 người')
                ->where('status',0)
                ->wheredate('date','=',date('Y-m-d'))->get();

            $khunggio5=Bill::select('khunggio')->where('truong','like',$diadiem)
                ->Where('loaisan','like','5 người')
                ->where('status',0)
                ->wheredate('date','=',date('Y-m-d'))->get();

            $khunggio7=Bill::select('khunggio')->where('truong','like',$diadiem)
                ->Where('loaisan','like','7 người')
                ->where('status',0)
                ->wheredate('date','=',date('Y-m-d'))->get();
            $data=[];
            if(count($khunggio11)>0){
                foreach ($khunggio11 as $item){
                    array_push($data,$item->khunggio);
                }

            }
            if(count($khunggio5)>0){
                foreach ($khunggio5 as $item){
                    array_push($data,$item->khunggio);
                }

            }
            if(count($khunggio7)>0){
                foreach ($khunggio7 as $item){
                    array_push($data,$item->khunggio);
                }

            }

            $khunggio=Khunggio::whereNotIn('id',$data)->get();
        }

        if(strcmp("5 người",$loaisan)==0){
            $khunggio115=Bill::select('khunggio')->where('truong','like',$diadiem)
                ->where('loaisan','like','11 người')
                ->where('status',0)
                ->wheredate('date','=',date('Y-m-d'))->get();

            $khunggio55=Khunggio::whereNotIn('id',$khunggio115)->get();
            $result1=[];
           foreach ($khunggio55 as $item){
               $khunggio45=Bill::select('khunggio')->where('truong','like',$diadiem)
                   ->where('loaisan','like','5 người')
                   ->where('status',0)
                   ->where('khunggio',$item->id)
                   ->wheredate('date','=',date('Y-m-d'))->get();

               $khunggio27=Bill::select('khunggio')->where('truong','like',$diadiem)
                   ->where('loaisan','like','7 người')
                   ->where('status',0)
                   ->where('khunggio',$item->id)
                   ->wheredate('date','=',date('Y-m-d'))->get();

               if(count($khunggio27)==2){
                  foreach ($khunggio27 as $item){
                      array_push($result1,$item->khunggio);
                  }
               }
               if(count($khunggio45)==4){
                   foreach ($khunggio45 as $item){
                       array_push($result1,$item->khunggio);
                   }
               }
               if(count($khunggio115)>0){
                   foreach ($khunggio115 as $item){
                       array_push($result1,$item->khunggio);
                   }
               }
           }
            $khunggio=Khunggio::whereNotIn('id',$result1)->get();


        }
        if(strcmp("7 người",$loaisan)==0){
            $khunggio117=Bill::select('khunggio')->where('truong','like',$diadiem)
                ->where('loaisan','like','11 người')
                ->where('status',0)
                ->wheredate('date','=',date('Y-m-d'))->get();

            $khunggio77=Khunggio::whereNotIn('id',$khunggio117)->get();
            $result2=[];

            foreach ($khunggio77 as $item){
                $khunggio35=Bill::select('khunggio')->where('truong','like',$diadiem)
                    ->where('loaisan','like','5 người')
                    ->where('status',0)
                    ->where('khunggio',$item->id)
                    ->wheredate('date','=',date('Y-m-d'))->get();

                $khunggio27=Bill::select('khunggio')->where('truong','like',$diadiem)
                    ->where('loaisan','like','7 người')
                    ->where('status',0)
                    ->where('khunggio',$item->id)
                    ->wheredate('date','=',date('Y-m-d'))->get();

                if(count($khunggio27)==4){
                    foreach ($khunggio27 as $item){
                        array_push($result2,$item->khunggio);
                    }
                }
                if(count($khunggio35)==3){
                    foreach ($khunggio35 as $item){
                        array_push($result2,$item->khunggio);
                    }
                }
                if(count($khunggio117)>0){
                    foreach ($khunggio117 as $item){
                        array_push($result2,$item->khunggio);
                    }
                }
            }
            $khunggio=Khunggio::whereNotIn('id',$result2)->get();

        }
        return response()->json([
            'khunggio'=>$khunggio,
        ]);
    }

    public function getNameKhunggio(Request $request)
    {
        $name=Khunggio::where('id',$request->id)->get();
        return response()->json($name);
    }


    public function getDatsan(){
            $data = DB::table('truongs')
                ->join('donhang', 'donhang.truong', '=', 'truongs.name')
                ->select('donhang.*', 'truongs.images')
                ->where('donhang.status',0)
                ->wheredate('donhang.date','=',date('Y-m-d'))
                ->orderBy('donhang.id','desc')->limit(5)->get();
            return response()->json($data);
    }

    public function getTin()
    {
        $tin=Tin::orderBy('id','desc')->limit(10)->get();
        return view('site.pages.listtin',compact('tin'));
    }
    public function getDetailTin(Request $request){
        $tin=Tin::where('id',$request->id)->get();
        return view('site.pages.detailtin',compact('tin'));
    }


}
