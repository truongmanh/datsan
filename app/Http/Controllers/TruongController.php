<?php

namespace App\Http\Controllers;

use App\Loaisan;
use App\Truong;
use Illuminate\Http\Request;
use file;

class TruongController extends Controller
{

    public function index()
    {
        $ls=Loaisan::all();
        $truong=Truong::orderBy('id','desc')->get();
        return view('admin.pages.truong.list',compact('ls','truong'));
    }

    public function getList()
    {
        $ls=Loaisan::all();
        $truong=Truong::orderBy('id','desc')->get();
        return view('admin.pages.truong.listajax',compact('ls','truong'));
    }

    public function addTruong(Request $request){

        try{

            $file=$request->file('txt_image');
            $truong = new Truong();
            $truong->name=$request->txt_name;
            $truong->status=$request->slt_status;
            $truong->mota=$request->txt_mota;
            if(strlen($file)>0){
                $filename=time().'.'.$file->getClientOriginalExtension();
                $path="uploads/";
                $file->move($path,$filename);
                $truong->images=$filename;
            }
            $truong->save();
            $truong->loaisans()->sync($request->slt_loaisan);

            return $this->getList();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function getDel(Request $request)
    {
        $truong=Truong::find($request->id);
        $truong->delete();
        return $this->getList();
    }

    public function getEdit(Request $request)
    {
        $truong=Truong::with('loaisans')->where('id',$request->id)->get();
        $ls=Loaisan::all();
        return response()->json([
            'truong'=>$truong,
            'loai'=>$ls,
        ]);
    }

    public function postEdit(Request $request){

        try{
            $file=$request->file('txt_image');
            $truong = Truong::find($request->txt_id);
            $truong->name=$request->txt_name;
            $truong->status=$request->slt_status;
            $truong->mota=$request->txt_mota;
            if(strlen($file)>0){
                $filename=time().'.'.$file->getClientOriginalExtension();
                $path="uploads/";
                $file->move($path,$filename);
                $truong->images=$filename;
            }
            else{
                $truong->images=$request->txt_imagepre;
            }
            $truong->update();
            $truong->loaisans()->sync($request->slt_loaisan);
            return $this->getList();
        }
        catch (\Exception $e){
            return $e->getMessage();
        }
    }
}
