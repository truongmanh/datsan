<?php

namespace App\Http\Controllers;

use App\Tin;
use Illuminate\Http\Request;
use file;

class TinController extends Controller
{
    public function index(){
        $tin=Tin::orderBy('id','desc')->get();
        return view('admin.pages.tin.list',compact('tin'));
    }
    public function getList(){
        $tin=Tin::orderBy('id','desc')->get();
        return view('admin.pages.tin.listajax',compact('tin'));
    }
    public function addTin(Request $request)
    {
        $file=$request->file('txt_image');
        $tin=new Tin();
        $tin->title=$request->txt_tieude;
        $tin->mota=$request->txt_mota;
        $tin->status=$request->slt_status;
        if(strlen($file)>0){
            $filename=time().'.'.$file->getClientOriginalExtension();
            $path="uploads/tin/";
            $file->move($path,$filename);
            $tin->image=$filename;
        }
        $tin->save();
        return $this->getList();
    }

    public function getEdit(Request $request){
        $tin=Tin::where('id',$request->id)->get();
        return response()->json($tin);
    }

    public function postTin(Request $request){
        $file=$request->file('txt_image');
        $file_pre=$request->txt_imagepre;
        $tin=Tin::find($request->id);
        $tin->title=$request->txt_tieude;
        $tin->mota=$request->txt_mota;
        $tin->status=$request->slt_status;
        if(strlen($file)>0){
            $filename=time().'.'.$file->getClientOriginalExtension();
            $path="uploads/tin/";
            $file->move($path,$filename);
            $tin->image=$filename;
        }
        else{
            $tin->image=$file_pre;
        }
        $tin->update();
        return $this->getList();
    }

    public function getdelete(Request $request){
        $id=$request->id;
        $tin=Tin::find($id);
        $tin->delete();
        return $this->getList();
    }
}
