<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddKhunggioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'txt_thoigian'=>'required',
            'slt_status'=>'required'
        ];
    }

    public function messages()
    {
        return [
          'txt_thoigian.required'=>'Thời gian không được để trống',
          'slt_status'=>'Trạng thái không được để trống'
        ];
    }
}
