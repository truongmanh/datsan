<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditLoaisanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txt_name'=>'required',
            'slt_status'=>'required'
        ];
    }

    public function messages()
    {
        return[
            'txt_name.required'=>'Tên không được để trống',
//            'txt_name.unique'=>'Tên không được để trùng',
            'slt_status.required'=>'Trạng thái không được để trống'
        ];
    }
}
