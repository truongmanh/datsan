<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loaisan extends Model
{
   protected $table="loaisans";

    public function truongs()
    {
        return $this->belongsToMany('App\Truong');
    }
}
