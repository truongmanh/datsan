<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',['as'=>'/','uses'=>'HomeController@index']);
Route::get('chi-tiet/{id}/{id_loai}',['as'=>'chitiet','uses'=>'HomeController@getDetail']);
Route::get('truong/{id}/{name?}',['as'=>'truong','uses'=>'HomeController@getTruong']);
Route::get('loaisan/{id}',['as'=>'loai-san','uses'=>'HomeController@getLoaisan']);
Route::post('thanh-toan',['as'=>'thanhtoan','uses'=>'HomeController@getDathang']);
Route::get('khung-gio',['as'=>'khunggio','uses'=>'HomeController@getKhunggio']);
Route::get('get-khung-gio',['as'=>'getkhunggio','uses'=>'HomeController@getNameKhunggio']);
Route::get('get-dat-san',['as'=>'getdatsan','uses'=>'HomeController@getDatsan']);
Route::get('tin-tuc',['as'=>'tin','uses'=>'HomeController@getTin']);
Route::get('chi-tiet-tin-tuc/{id}/{name?}',['as'=>'chtin','uses'=>'HomeController@getDetailTin']);

Route::get('ql_admin/login',['as'=>'login','uses'=>'AdminController@getLogin']);
Route::post('ql_admin/postlogin',['as'=>'postlogin','uses'=>'AdminController@postLogin']);
Route::get('ql_admin/logout',['as'=>'getlogout','uses'=>'AdminController@getLogout']);

Route::group(['middleware'=>'auth'],function (){
Route::prefix('ql_admin')->group(function () {
    Route::get('/',['as'=>'getdas','uses'=>'AdminController@index']);
    Route::prefix('truong')->group(function () {
        Route::get('danh-sach',['as'=>'ds-truong','uses'=>'TruongController@index']);
        Route::post('them-truong',['as'=>'themtruong','uses'=>'TruongController@addTruong']);
        Route::get('xoa-truong',['as'=>'xoatruong','uses'=>'TruongController@getDel']);
        Route::get('get-edit-truong',['as'=>'gettruong','uses'=>'TruongController@getEdit']);
        Route::post('post-edit-truong',['as'=>'posttruong','uses'=>'TruongController@postEdit']);

    });
    Route::prefix('loaisan')->group(function () {
        Route::get('danh-sach',['as'=>'ds-loaisan','uses'=>'LoaisanController@index']);
        Route::post('them-loai-san',['as'=>'addloaisan','uses'=>'LoaisanController@create']);
        Route::get('xoa-loai-san',['as'=>'delloaisan','uses'=>'LoaisanController@delete']);
        Route::get('get-loai-san',['as'=>'getloaisan','uses'=>'LoaisanController@getUpdated']);
        Route::post('post-loai-san',['as'=>'postloaisan','uses'=>'LoaisanController@postUpdated']);
        Route::get('get-add-san',['as'=>'getaddloaisan','uses'=>'LoaisanController@getAddloaisan']);
    });
    Route::prefix('thoigian')->group(function () {
        Route::get('danh-sach',['as'=>'ds-thoigian','uses'=>'ThoigianController@index']);
        Route::post('them-khung-gio',['as'=>'addkhunggio','uses'=>'ThoigianController@addTime']);
        Route::get('xoa-khung-gio',['as'=>'xoakhunggio','uses'=>'ThoigianController@delete']);
        Route::get('get-khung-gio',['as'=>'getkhunggio','uses'=>'ThoigianController@getEdit']);
        Route::post('post-khung-gio',['as'=>'postkhunggio','uses'=>'ThoigianController@postEdit']);
    });
    Route::prefix('tintuc')->group(function () {
        Route::get('danh-sach',['as'=>'tdanhsach','uses'=>'TinController@index']);
        Route::post('them-tin',['as'=>'addtin','uses'=>'TinController@addTin']);
        Route::get('xoa-tin',['as'=>'xoatin','uses'=>'TinController@getdelete']);
        Route::get('get-tin',['as'=>'gettin','uses'=>'TinController@getEdit']);
        Route::post('sua-tin',['as'=>'posttin','uses'=>'TinController@postTin']);
    });
    Route::prefix('users')->group(function () {
        Route::get('danh-sach',["as"=>'udanhsach',"uses"=>"UserController@index"]);
        Route::post('add-user',["as"=>'adduser',"uses"=>"UserController@addUser"]);
        Route::get('del-user',["as"=>'deluser',"uses"=>"UserController@delUser"]);

    });
    Route::prefix('donhang')->group(function () {
        Route::get('danh-sach',["as"=>'dhdanhsach',"uses"=>"DonhangController@index"]);
        Route::post('edit-donhang',["as"=>'editdh',"uses"=>"DonhangController@postDonhang"]);
    });
});
});
